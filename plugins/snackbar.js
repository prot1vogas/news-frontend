export default ({app, store}, inject) => {
    const snackbar = {
        show(message, type) {
            store.commit('snackbar/show', {message, type});
        },
        hide() {
            store.commit('snackbar/hide');
        }
    };

    inject('snackbar', snackbar);
}