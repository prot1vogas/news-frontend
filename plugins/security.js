export default ({app, store, $axios}, inject) => {
    const security = {
        roleUser: 'ROLE_USER',
        roleAdmin: 'ROLE_ADMIN',
        isHasRole(role) {
            return this.isLoggedIn() && this.getUser().roles.includes(role);
        },
        isLoggedIn() {
            return this.getUser().id !== null;
        },
        isOwner({user: {id}}) {
            return this.isLoggedIn() && this.getUser().id === id;
        },
        getUser() {
            return store.state.user.user;
        },
        login({token, user}) {
            localStorage.token = token;
            store.dispatch('user/setUser', user);
            store.dispatch('user/setToken', token);
        },
        logout() {
            localStorage.removeItem('token');
            store.dispatch('user/logout');
        },
        async init() {
            if (localStorage.token) {
                store.dispatch('user/setToken', localStorage.token);
                $axios.$get('/user/me').then(response => {
                    store.dispatch('user/setUser', response.data);
                }).catch(() => {
                    this.logout();
                });
            } else {
                this.logout();
            }
        }
    };
    if (process.client) {
        security.init();
    }
    inject('security', security);
}
