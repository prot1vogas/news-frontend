module.exports = {
    /*
    ** Headers of the page
    */
    head: {
        title: 'news-frontend',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'Nuxt.js project'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    /*
    ** Plugins
    */
    plugins: [
        '~plugins/filters',
        '~/plugins/axios',
        '~/plugins/security',
        '~/plugins/snackbar'
    ],
    /*
    ** Customize the progress bar color
    */
    loading: {color: '#3B8070'},
    /*
    ** Build configuration
    */
    build: {
        /*
        ** Run ESLint on save
        */
        extend(config, {isDev, isClient}) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    },
    buildModules: [
        '@nuxtjs/vuetify',
        ['@nuxtjs/axios', {baseURL: 'http://localhost:8018'}],
    ],
    router: {
        middleware: ['auth']
    }
};
