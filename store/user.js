export const state = () => ({
    token: '',
    user: {
        id: null,
        name: '',
        email: '',
        roles: []
    }
});

export const mutations = {
    setToken(state, token) {
        state.token = token;
    },
    setUser(state, {id, name, email, roles}) {
        state.user.id = id;
        state.user.name = name;
        state.user.email = email;
        state.user.roles = roles;
    },
    clear(state) {
        state.token = '';
        state.user.id = null;
        state.user.name = '';
        state.user.email = '';
        state.user.roles = [];
    }
};

export const actions = {
    setUser({ commit }, user) {
        commit('setUser', user);
    },
    setToken({ commit }, token) {
        commit('setToken', token);
    },
    logout({ commit }) {
        commit('clear');
    }
};
