export const state = () => ({
    message: '',
    type: '',
    show: false
});

export const mutations = {
    show(state, {message, type}) {
        state.message = message;
        state.type = type;
        state.show = true;
    },
    hide(state) {
        state.show = false;
    }
};
